package com.platzi.jobsearch.cli;

import com.beust.jcommander.Parameter;
import com.platzi.jobsearch.cli.validators.CLIHelpValidator;
import com.platzi.jobsearch.cli.validators.CLIKeywordValidator;

public class CLIArguments {

    CLIArguments() {
    }

    @Parameter(required = true,
            descriptionKey = "KEYWORD",
            description = "KEYWORD",
            validateWith =   CLIKeywordValidator.class)
    private String keyword;


    @Parameter(
            names = {"--location", "-l"},
            description = "Each search can be have a location"
    )
    private String location;

    @Parameter(names = {"--page", "-p"},
            description = "50 results by default, use a number for pagination")
    private int page = 0;

    @Parameter(names = {"--full-time", "-ft"},
            description = "Use for search full time jobs")
    private boolean isFullTime = false;

    @Parameter(names = {"--markdown", "-mk"},
            description = "get markdown results")
    private boolean isMarkdown = false;

    @Parameter(names = {"--help", "-h"},
            description = "show help",
            help = true,
            validateWith = CLIHelpValidator.class)
    private boolean isHelp;

    public String getKeyword() {
        return keyword;
    }

    public String getLocation() {
        return location;
    }

    public int getPage() {
        return page;
    }

    public boolean isFullTime() {
        return isFullTime;
    }

    public boolean isMarkdown() {
        return isMarkdown;
    }

    public boolean isHelp() {
        return isHelp;
    }

    @Override
    public String toString() {
        return "CLIArguments{" +
                "keyword='" + keyword + '\'' +
                ", location='" + location + '\'' +
                ", page=" + page +
                ", isFullTime=" + isFullTime +
                ", isMarkdown=" + isMarkdown +
                ", isHelp=" + isHelp +
                '}';
    }

    public static CLIArguments newInstance() {
        return new CLIArguments();
    }
}
