package com.platzi.jobsearch;

import com.beust.jcommander.JCommander;
import com.platzi.jobsearch.api.APIJobs;
import com.platzi.jobsearch.api.ApiFunctions;
import com.platzi.jobsearch.cli.CLIArguments;
import com.platzi.jobsearch.cli.CLIFunctions;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class JobSearch {
    public static void main(String[] args) {
        JCommander jCommander = CommanderFunctions.buildCommanderWithName("job-search",
                CLIArguments::newInstance);
        Stream<CLIArguments> arguments = CommanderFunctions.parseArguments(jCommander, args, JCommander::usage)
                .orElse(Collections.emptyList())
                .stream()
                .map(cliArg -> (CLIArguments) cliArg);

        Optional<CLIArguments> argumentsOptional = arguments.filter(cliArgument -> !cliArgument.isHelp())
                .filter(cliArgument -> cliArgument.getKeyword() != null && !cliArgument.getKeyword().isEmpty())
                .findFirst();

        argumentsOptional.map(CLIFunctions::toMap)
                .map(JobSearch::executeRequest)
                .orElse(Stream.empty())
                .forEach(System.out::println);
    }

    private static Stream<JobPosition> executeRequest(Map<String, Object> params) {
        APIJobs api = ApiFunctions.buildAPI(APIJobs.class, "https://jobs.github.com");
        return Stream.of(params).map(api::jobs  ).flatMap(Collection::stream);
    }

}
